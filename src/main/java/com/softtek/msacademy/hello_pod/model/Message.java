package com.softtek.msacademy.hello_pod.model;

import java.time.LocalDateTime;

public class Message {

    private String message;
    private String response;
    private LocalDateTime time;
    private int requestcounter;

    public Message(String message, String response, LocalDateTime time, int requestcounter){
        this.message = message;
        this.response = response;
        this.time = time;
        this.requestcounter = requestcounter;
    }

    public LocalDateTime getTime(){
        return time;
    }

    public String getMessage(){
        return message;
    }

    public int getRequestCounter(){
        return requestcounter;
    }

    public String getResponse(){
        return response;
    }

}