package com.softtek.msacademy.hello_pod.controller;

import java.time.LocalDateTime;

import com.softtek.msacademy.hello_pod.model.Message;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class helloPodController {

    LocalDateTime time = LocalDateTime.now();
    int request_counter = 1;
    
    @GetMapping("/yesimessage")
    public ResponseEntity<?> sayHello() {
        return new ResponseEntity<>(new Message("Hey!! nice to see you here!", HttpStatus.OK.toString(), time, request_counter++), HttpStatus.OK);
    }

}